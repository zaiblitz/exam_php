<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Employees_model extends CI_Model
{

	private $tableName = 'employees';

	public function employees_org_structure() {
        $this->db->select('employeeNumber');
        $this->db->select('CONCAT(employees.firstName," ", employees.lastName) AS name');
        $this->db->select('jobTitle');
        $this->db->select('reportsTo');
        $this->db->from($this->tableName);
        $query = $this->db->get();
        $result = $query->result_array();
        return $this->organizational_chart($result);
	}


	public function organizational_chart(array $elements, $parentId = null) {
	    $data = array();
	    foreach ($elements as $element) {
	        if ($element['reportsTo'] == $parentId) {
	            $children = $this->organizational_chart($elements, $element['employeeNumber']);
	            if ($children) {
	                $element['employeeUnder'] = $children;
	            }
                unset($element['reportsTo']);
	            $data[] = $element;
	        }
	    }
	    return $data;
	}


	public function office_employee_details() {
		$this->db->select('offices.officeCode, offices.city');
        $this->db->select('employees.employeeNumber');
        $this->db->select('employees.jobTitle');
        $this->db->select('CONCAT(employees.firstName," ", employees.lastName) AS name');
        $this->db->from('offices');
        $this->db->join('employees','employees.officeCode = offices.officeCode','left');
        $query = $this->db->get();
        $offices = $query->result_array();

        $data = array();
        if (is_array($offices)) {
        	foreach ($offices as $key => $office) {
    			$data[$office['officeCode']]['officeCode'] =  $office['officeCode'];
    			$data[$office['officeCode']]['city'] =  $office['city'];

    			if(isset($data[$office['officeCode']]['officeCode'])) {
    				$employees = [
    					'employeeNumber' => $office['employeeNumber'],
    					'name' => $office['name'],
    					'jobTitle' => $office['jobTitle'],
    				];
    				if(!isset($data[$office['officeCode']]['employees'])) {
    					$data[$office['officeCode']]['employees'] = array();
    				}
    				array_push($data[$office['officeCode']]['employees'], $employees);
    			}
        	}
        }
        return $data;
	}

	public function sales_report($employee_number) {
        		
	}
}