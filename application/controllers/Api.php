<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

	public function __construct() 
	{        
    	parent::__construct();
    	$this->load->model('employees_model');

	}

	public function organization() 
	{
		$chart = $this->employees_model->employees_org_structure();
		$this->output->set_content_type('application/json')->set_output(json_encode($chart));
	}

	public function offices() 
	{
		$office = $this->employees_model->office_employee_details();
		$this->output->set_content_type('application/json')->set_output(json_encode($office));
	}

	public function sales_report($employee_number) 
	{
		$report = $this->employees_model->sales_report();
		$this->output->set_content_type('application/json')->set_output(json_encode($report));
	}
}
